import json
import pathlib
import requests
from math import sin, cos, sqrt, atan2, radians

from config import Common


# Get customer list from the URL
def getCustomList():
    response = requests.get(Common.customerListUrl)
    if response.status_code == 200:
        for customer in response.text.split('\n'):
            customerList.append(json.loads(customer))
    else:
        with open(
                pathlib.Path(__file__).parent / '../resources/customers.txt', 'r') as data:
            for customer in data:
                customerList.append(json.loads(customer.strip()))
    return sorted(customerList, key=lambda i: i['user_id'])


# Get distance using latitude and longitude
def getDistance(latitude, longitude):
    officeLatitude = radians(Common.officeLatitude)
    officeLongitude = radians(Common.officeLongitude)
    customerLatitude = radians(latitude)
    customerLongitude = radians(longitude)
    lon = customerLongitude - officeLongitude
    lat = customerLatitude - officeLatitude
    a = sin(lat / 2) ** 2 + cos(officeLatitude) * cos(customerLatitude) * sin(lon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    return Common.earthRadius * c


if __name__ == '__main__':
    customerList = []
    output = []
    for customers in getCustomList():
        # customer list within 100 kms radius
        if getDistance(float(customers['latitude']), float(customers['longitude'])) < 100:
            del customers['latitude'], customers['longitude']
            output.append(customers)
    print("Result:", output)
