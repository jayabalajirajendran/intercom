# Calculate distance using Latitude and Longitude

We have some customer records in a text file (customers.txt) -- one customer per line, JSON lines formatted. 
We want to invite any customer within 100 km of our Dublin office for some food and drinks on us.
Write a program that will read the full list of customers and output the names and user ids of matching customers 
(within 100 km), sorted by User ID (ascending).

## Getting Started
* You should need a python interpreter to run the following code. Please see installation below for the notes if you don't have python in your system.
* You can [run](https://realpython.com/run-python-scripts/) the Main.py file to get the output of the program using the command
```
   python3 Main.py
```
* Unit Test cases are under the resources folder, you can run the TestCase.py file to get the test cases output results.
```
   python3 TestCase.py
```

### Prerequisites
* The whole project is set up using [Intellij Idea](https://www.jetbrains.com/idea/) IDE. You can either try using it or your convenient way to run the program.

## Installation
In Mac, You can install python 3.8 using [Home Brew](https://brew.sh)
```
    brew install python@3.8 
```
If Home Brew isn't installed in your system, please use the below code first before installing python 3.8
```
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

Project Organization
------------

    ├── README.md            <- The top-level README for developers using this project.
    ├── config
    │   └── Common.py        <-  Making a common file to have generic data.
    │
    ├── resources         
    │   ├── test             <- Unit Test cases for this project
    │       └── TestCase.py  
    │   └── customer.txt     <- Static file as a backup if there is a failure in API.
    │   └── outputs.txt      <- Output of this project for your reference.
    │
    └── Main.py              <- Main file to get output of the project.

--------

## Technologies Used
* [Python](https://www.python.org) 
* [Intellij Idea IDE](https://www.jetbrains.com/idea/) 

## Versioning

Project Version = 0.0.1 

## BitBucket

The project is uploaded in [Bitbucket](https://bitbucket.org/jayabalajirajendran/intercom/src/master/)

## Reference
Please read https://en.wikipedia.org/wiki/Great-circle_distance for details on haversine formula.