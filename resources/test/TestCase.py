import json
from math import sin, cos, sqrt, atan2, radians

import requests
import unittest

import Main
from config import Common


class TestStringMethods(unittest.TestCase):

    def test_customerList(self):
        response = requests.get('https://s3.amazonaws.com/intercom-take-home-test/customers.txt')
        self.assertEqual(response.status_code, 200)

    def test_calculation_response(self):
        officeLatitude = radians(Common.officeLatitude)
        officeLongitude = radians(Common.officeLongitude)
        customerLatitude = radians(53.2451022)
        customerLongitude = radians(-6.238335)
        lon = customerLongitude - officeLongitude
        lat = customerLatitude - officeLatitude
        a = sin(lat / 2) ** 2 + cos(officeLatitude) * cos(customerLatitude) * sin(lon / 2) ** 2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))
        distance = Common.earthRadius * c
        self.assertEqual(distance, 10.57025348751525)

    def test_within_radius(self):
        customerList = []
        response = requests.get(Common.customerListUrl)
        if response.status_code == 200:
            for customer in response.text.split('\n'):
                customerList.append(json.loads(customer))
            customerList = sorted(customerList, key=lambda i: i['user_id'])
            for customers in customerList:
                if Main.getDistance(float(customers['latitude']), float(customers['longitude'])) < 100:
                    self.assertTrue(True)
        else:
            self.assertTrue(False)


if __name__ == "__main__":
    unittest.main()
